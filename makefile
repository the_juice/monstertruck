CFLAGS = `pkg-config --cflags opencv`
LIBS = `pkg-config --libs opencv`

monsterTruck : main.cpp
	g++ $(CFLAGS) $(LIBS) -o $@ -pthread -std=c++11 $<

clean :
	-rm monsterTruck
